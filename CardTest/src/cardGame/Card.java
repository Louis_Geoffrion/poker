package cardGame;

public class Card {
   public char face;
   public char suit;
   public short num;
   Card (int faces, int suits)
   {
	   char faceList[] = {'A','2','3','4','5','6','7','8',
			   '9','T','J','Q','K'};
	   char suitList[] = {'S','D','C','H'};
	   face = faceList[faces];
	   suit = suitList[suits];
	   num = (short)faces;
   }
   public void show()
   {
	   System.out.print(face+"" + suit + " " );
   }
}
