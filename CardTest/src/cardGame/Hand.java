package cardGame;
import java.util.Vector;

public abstract class Hand {
	public short rank;
	public Vector<Card> cards = new Vector<Card>();
	public Card Highest;
}
