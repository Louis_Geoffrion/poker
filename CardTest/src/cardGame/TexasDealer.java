package cardGame;
import java.lang.Math;
public class TexasDealer{
	public Board board = new Board();
	public Deck deck = new Deck();
	TexasDealer(int gameSize)
	{
		for (short count=0;count < gameSize; count++)
			board.players.add(new Player());
	}
	public void dealCards(short gameSize)
	{
		//  Deal Cards to the players  
		for (short twoCards=0;twoCards<2;twoCards++)
			for (short count=0;count < gameSize;count++)
				board.players.elementAt(count).addCard(deck.flipCard());
		
		for (short count=0;count<3;count++)
			board.table.add(deck.flipCard());
	}
	
	public void nextCard()	
	{
		board.table.add(deck.flipCard());		
	}
	public void checkPairs()
	{
		for (int count=0;count<board.table.size();count++)
			for (int i=(count+1);i<board.table.size();i++)
			{
				// pairs
				if (board.table.elementAt(count).face == board.table.elementAt(count).face )
					board.relations.add(new Relation(board.table.get(count),board.table.get(i),"pair"));
				// suits
				if (board.table.elementAt(count).suit == board.table.elementAt(count).suit )
					board.relations.add(new Relation(board.table.get(count),board.table.get(i),"suit"));
				// continual
				if (1 == Math.abs(board.table.elementAt(count).num - board.table.elementAt(count).num) )
					board.relations.add(new Relation(board.table.get(count),board.table.get(i),"cont"));
			}
			
	}	
}
