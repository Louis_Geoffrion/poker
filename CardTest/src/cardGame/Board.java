package cardGame;
import java.util.Vector;

public class Board {
	public Vector <Relation> relations = new Vector<Relation>();
	public Vector <Player> players = new Vector <Player>();
	public Vector <Card> table = new Vector<Card>();
	public void showBoard()
	{
		for (short count=0;count<players.size();count++)
		{
			System.out.print("player"+count+": ");
			players.elementAt(count).showHand();
			System.out.print("\n");
		}
		System.out.println("The Board: ");
		for (short count=0;count<table.size();count++)
			table.elementAt(count).show();
		System.out.print("\n");
	}
}
