package cardGame;

import java.util.Enumeration;
import java.util.Vector;
import java.util.Stack;
import java.util.concurrent.ThreadLocalRandom;

public class Deck {
	private Stack<Card> cards = new Stack<Card>();
	private Vector<Card> turnedCards = new Vector<Card>();
	Deck () 
	{
		for (int suits = 0; suits < 4; suits++)
			for (int faces = 0; faces <13; faces ++)
			{
				Card holder = new Card(faces,suits);
				cards.add(holder);
			}
	}

	//public void printDeck()
	//{
	//	for (Enumeration e = cards.elements(); e.hasMoreElements();)
	//	{
	//		Card here = (Card)e.nextElement();
	//		here.show();
	//	}	
	//}
	
	public void shuffle()
	{
			turnedCards.addAll(cards);
			while(!cards.isEmpty())
				cards.pop();
			while (!turnedCards.isEmpty())
				cards.add(turnedCards.remove(ThreadLocalRandom.current().nextInt(0, turnedCards.size())));
	}
	public Card flipCard()
	{
		Card temp = (Card)cards.pop();
		turnedCards.add(temp);
		return temp;
	}
}

