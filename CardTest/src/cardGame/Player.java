package cardGame;
import java.util.Vector;

public class Player {
	int cash;
	short score;
	public Vector<Relation> relations = new Vector<Relation>();
	public Vector<Card> cards = new Vector<Card>();
	public void addCard(Card toAdd) {cards.add(toAdd);}
	public void showHand()
	{
		for (short count=0;count<cards.size();count++)
			cards.elementAt(count).show();
	}
	//public void clearCards() {cards.clear();}
}
